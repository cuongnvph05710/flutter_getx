import 'package:get/get.dart';
import 'package:getx_parttern/data/repository/profile_repository.dart';
import 'package:getx_parttern/data/repository/repository.dart';

class AppBindings extends Bindings {
  @override
  void dependencies() {
    _injectController();
  }

  void _injectController() {
    ProfileRepository profileRepository = Get.put(ProfileRepository());
    Get.lazyPut(() => Repository(profileRepository));
  }
}
