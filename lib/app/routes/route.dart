import 'package:get/get.dart';
import 'package:getx_parttern/app/pages/home/home_bindings.dart';
import 'package:getx_parttern/app/pages/home/home_middleware.dart';
import 'package:getx_parttern/app/pages/home/home_view.dart';
import 'package:getx_parttern/app/pages/login/login_binding.dart';
import 'package:getx_parttern/app/pages/login/login_middleware.dart';
import 'package:getx_parttern/app/pages/login/login_view.dart';
import 'package:getx_parttern/app/pages/splash/splash_middleware.dart';
import 'package:getx_parttern/app/pages/splash/splash_view.dart';

abstract class Routes {
  static const HOME = "/home";
  static const LOGIN = "/login";
  static const SPLASH = "/splash";

  static final routes = [
    GetPage(
      name: Routes.SPLASH,
      page: () => SplashScreen(),
      middlewares: [SplashMiddleWare()]
    ),
    GetPage(
        name: Routes.HOME,
        page: () => HomePage(),
        middlewares: [HomeMiddleWare()],
        binding: HomeBindings()),
    GetPage(
        name: Routes.LOGIN,
        page: () => LoginPage(),
        binding: LoginBindings(),
        middlewares: [LoginMiddleWare()]),
  ];
}
