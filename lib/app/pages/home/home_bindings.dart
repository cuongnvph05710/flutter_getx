import 'package:get/get.dart';
import 'package:getx_parttern/app/pages/home/home_controller.dart';

class HomeBindings extends Bindings{
  @override
  void dependencies() {
   Get.lazyPut(()=>HomeController());
  }

}