import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_parttern/app/pages/home/home_controller.dart';

class HomePage extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("HomePage")),
      body: Obx(()=>Text(controller.count.toString())),
      floatingActionButton: FloatingActionButton(
        onPressed: ()=> controller.incrementCount(),
      ),
    );
  }
}
