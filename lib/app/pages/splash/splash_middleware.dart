import 'package:get/get.dart';
import 'package:getx_parttern/app/pages/home/home_view.dart';
import 'package:getx_parttern/app/routes/route.dart';

class SplashMiddleWare extends GetMiddleware{
  @override
  onPageBuildStart(page) {
  _onNext();
    return super.onPageBuildStart(page);
  }
  _onNext(){
    Get.toNamed(Routes.HOME);
  }
}