
import 'package:get/get.dart';
import 'package:getx_parttern/data/repository/repository.dart';
import 'data/profile.dart';

class LoginController extends GetxController {
 var profile = ProfileModel().obs;
  var repository = Get.find<Repository>();
  Future login() {
    return repository.profileRepostory.getProfile().then((value) {
      profile.value = value;
    });
  }
}
