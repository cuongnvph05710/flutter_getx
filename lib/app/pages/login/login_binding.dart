import 'package:get/get.dart';
import 'package:getx_parttern/app/pages/login/login_controller.dart';

class LoginBindings extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut(() => LoginController());
  }

}