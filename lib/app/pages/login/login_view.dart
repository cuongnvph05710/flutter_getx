import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_parttern/app/pages/login/login_controller.dart';

class LoginPage extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(title: Text("Login")),
      body: Obx(() => Center(child: Text(controller.profile.value.fullname ?? ''))),
      floatingActionButton: FloatingActionButton(
        onPressed: () => controller.login(),
        child: Icon(Icons.add),
      ),
    );
  }
}
