import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_parttern/app/bindings/app_bindings.dart';
import 'package:getx_parttern/app/pages/home/home_view.dart';
import 'package:getx_parttern/app/routes/route.dart';

import 'app/pages/login/login_binding.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await Injection.initInjection();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialBinding: AppBindings(),
      initialRoute: Routes.LOGIN,
      theme: ThemeData.dark(),
      themeMode: ThemeMode.dark,
      defaultTransition: Transition.fade,
      getPages: Routes.routes,
      locale: Locale('vn', 'VN'),
    );
  }
}
